﻿using System;
using System.Collections.Generic;

namespace TestProject.Domain.DTOs
{
    public class ProductDTO {
        public Guid? Id { get; set; }
        public string PhotoUrl { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public ICollection<string> CategoryNames { get; set; }
        public ICollection<ParameterDTO> Parameters { get; set; }
    }
}
