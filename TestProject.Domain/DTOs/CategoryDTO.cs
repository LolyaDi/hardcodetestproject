﻿using System;
using System.Collections.Generic;

namespace TestProject.Domain.DTOs
{
    public class CategoryDTO {
        public Guid? Id { get;set; }
        public string Name { get; set; }
        public virtual ICollection<ParameterNameDTO> ParameterNames { get; set; }
    }
}
