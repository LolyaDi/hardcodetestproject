﻿using System;

namespace TestProject.Domain.DTOs
{
    public class ParameterNameDTO {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
