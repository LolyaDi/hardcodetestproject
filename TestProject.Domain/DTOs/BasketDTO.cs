﻿using System;
using System.Collections.Generic;

namespace TestProject.Domain.DTOs
{
    public class BasketDTO {
        public Guid UserId { get; set; }
        public ICollection<ProductDTO> Products { get; set; }
    }
}
