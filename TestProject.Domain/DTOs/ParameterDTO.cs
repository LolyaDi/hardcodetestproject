﻿using System;

namespace TestProject.Domain.DTOs
{
    public class ParameterDTO {
        public string CategoryName { get; set; }

        public Guid ParameterNameId { get; set; }
        public string ParameterName { get; set; }

        public string ParameterValue { get; set; }
    }
}
