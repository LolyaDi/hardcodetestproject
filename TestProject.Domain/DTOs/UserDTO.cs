﻿using System;

namespace TestProject.Domain.DTOs
{
    public class UserDTO {
        public Guid? Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
