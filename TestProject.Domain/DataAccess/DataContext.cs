﻿using Microsoft.EntityFrameworkCore;
using TestProject.Domain.Models;

namespace TestProject.Domain.DataAccess
{
    public class DataContext: DbContext {
        public DataContext(DbContextOptions<DataContext> options) : base(options) {}

        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ParameterName> ParameterNames { get; set; }
        public DbSet<ParameterValue> ParameterValues { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Basket> Baskets { get; set; }
    }
}
