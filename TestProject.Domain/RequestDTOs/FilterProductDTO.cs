﻿using System.Collections.Generic;

namespace TestProject.Domain.RequestDTOs
{
    public class FilterProductDTO {
        public ICollection<string> CategoryNames { get; set; }
        public IDictionary<string, string> Parameters { get; set; }
    }
}
