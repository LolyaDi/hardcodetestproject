﻿using System;

namespace TestProject.Domain.RequestDTOs
{
    public class ProductBasketRequestDTO {
        public Guid UserId { get; set; }
        public Guid ProductId { get; set; }
    }
}
