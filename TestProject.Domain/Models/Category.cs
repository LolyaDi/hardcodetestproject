﻿using System.Collections.Generic;

namespace TestProject.Domain.Models
{
    public class Category: Entity {
        public string Name { get; set; }
        public virtual ICollection<Product> Products { get; set; } = new List<Product>();
        public virtual ICollection<ParameterName> ParameterNames { get; set; } = new List<ParameterName>();
    }
}
