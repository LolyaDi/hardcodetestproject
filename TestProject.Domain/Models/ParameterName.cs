﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestProject.Domain.Models
{
    public class ParameterName: Entity {
        public string Name { get; set; }
        
        [ForeignKey("Category")]
        public Guid CategoryId { get; set; }
        public virtual Category Category { get; set; }
    }
}
