﻿using System.Collections.Generic;

namespace TestProject.Domain.Models
{
    public class Product: Entity {
        public string PhotoUrl { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public virtual ICollection<Category> Categories { get; set; } = new List<Category>();
        public virtual ICollection<ParameterValue> ParameterValues { get; set; } = new List<ParameterValue>();

        public virtual ICollection<Basket> Baskets { get; set; }
    }
}
