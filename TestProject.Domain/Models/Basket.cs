﻿using System.Collections.Generic;

namespace TestProject.Domain.Models
{
    public class Basket: Entity {
        public virtual User User { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
