﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestProject.Domain.Models
{
    public class User: Entity {
        public string Username { get; set; }
        public string Password { get; set; }

        [ForeignKey("Basket")]
        public Guid BasketId { get; set; }
        public virtual Basket Basket { get; set; }
    }
}
