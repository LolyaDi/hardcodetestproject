﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestProject.Domain.Models
{
    public class ParameterValue: Entity {
        public string Value { get; set; }

        [ForeignKey("ParameterName")]
        public Guid ParameterNameId { get; set; }
        public virtual ParameterName ParameterName { get; set; }
        
        [ForeignKey("Product")]
        public Guid ProductId { get; set; }
        public virtual Product Product { get; set; }
    }
}
