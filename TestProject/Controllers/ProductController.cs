﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TestProject.Application.IServices;
using TestProject.Domain.DTOs;
using TestProject.Domain.RequestDTOs;

namespace TestProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase {
        private readonly IProductService _productService;

        public ProductController(IProductService productService) {
            _productService = productService;
        }

        [HttpGet("/allProducts")]
        public async Task<IActionResult> GetProducts() {
            var products = await _productService.GetProducts();
            return Ok(products);
        }

        [HttpPost("/filterProducts")]
        public async Task<IActionResult> FilterProducts([FromBody]FilterProductDTO filterDTO) {
            var filteredProducts = await _productService.GetFilteredProducts(filterDTO);
            return Ok(filteredProducts);
        }

        [HttpPost]
        public async Task<IActionResult> AddProduct([FromBody]ProductDTO productDTO) {
            await _productService.AddProduct(productDTO);
            return Ok();
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteProduct(Guid id) {
            await _productService.DeleteProduct(id);
            return Ok();
        }
    }
}
