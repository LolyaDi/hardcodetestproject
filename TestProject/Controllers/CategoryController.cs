﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TestProject.Application.IServices;
using TestProject.Domain.DTOs;

namespace TestProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService) {
            _categoryService = categoryService;
        }

        [HttpGet]
        public async Task<IActionResult> GetCategories() {
            var categories = await _categoryService.GetCategories();
            return Ok(categories);
        }

        [HttpPost]
        public async Task<IActionResult> AddCategory([FromBody]CategoryDTO categoryDTO) {
            await _categoryService.AddCategory(categoryDTO);
            return Ok();
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteCategory(Guid id) {
            await _categoryService.DeleteCategory(id);
            return Ok();
        }
    }
}
