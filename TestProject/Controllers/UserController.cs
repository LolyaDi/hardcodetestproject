﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TestProject.Application.IServices;
using TestProject.Domain.DTOs;

namespace TestProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase {
        private readonly IUserService _userService;

        public UserController(IUserService userService) {
            _userService = userService;
        }

        [HttpGet]
        public async Task<IActionResult> GetUsers() {
            var users = await _userService.GetUsers();
            return Ok(users);
        }

        [HttpPost]
        public async Task<IActionResult> AddUser([FromBody]UserDTO userDTO) {
            await _userService.AddUser(userDTO);
            return Ok();
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteUser(Guid id) {
            await _userService.DeleteUser(id);
            return Ok();
        }
    }
}
