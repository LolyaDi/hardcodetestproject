﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using TestProject.Application.IServices;
using TestProject.Domain.RequestDTOs;

namespace TestProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BasketController : ControllerBase
    {
        private readonly IBasketService _basketService;

        public BasketController(IBasketService basketService) {
            _basketService = basketService;
        }

        [HttpGet("/allBaskets")]
        public async Task<IActionResult> GetBaskets() {
            var baskets = await _basketService.GetBaskets();
            return Ok(baskets);
        }

        [HttpGet("/allBasketProducts")]
        public async Task<IActionResult> GetBasketProducts(Guid id) {
            var products = await _basketService.GetProducts(id);
            return Ok(products);
        }

        [HttpPost]
        public async Task<IActionResult> AddBasketProduct([FromBody]ProductBasketRequestDTO requestDTO) {
            await _basketService.AddProduct(requestDTO);
            return Ok();
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteBasketProduct([FromBody]ProductBasketRequestDTO requestDTO) {
            await _basketService.DeleteProduct(requestDTO);
            return Ok();
        }
    }
}
