﻿using TestProject.Application.IServices;

namespace TestProject.Infrastructure.Services
{
    public class PasswordService: IPasswordService {
        public string HashPassword(string password) {
            return BCrypt.Net.BCrypt.HashPassword(password);
        }

        public bool IsVerifiedPassword(string password, string passwordHash) {
            return BCrypt.Net.BCrypt.Verify(password, passwordHash);
        }
    }
}
