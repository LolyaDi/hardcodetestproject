﻿using Ardalis.Specification.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestProject.Application.IServices;
using TestProject.Domain.DataAccess;
using TestProject.Domain.DTOs;
using TestProject.Domain.Models;
using TestProject.Domain.RequestDTOs;
using TestProject.Infrastructure.Specifications;

namespace TestProject.Infrastructure.Services
{
    public class BasketService: IBasketService {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public BasketService(DataContext context, IMapper mapper) {
            _context = context;
            _mapper = mapper;

            _context.ChangeTracker.LazyLoadingEnabled = false;
        }

        public async Task<ICollection<BasketDTO>> GetBaskets() {
            var specification = new EntityIsBeingDeletedFilterSpecification<User>();

            var result = await _context.Users.WithSpecification(specification)
                .ProjectTo<BasketDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
            return result;
        }

        public async Task<ICollection<ProductDTO>> GetProducts(Guid id) {
            var specification = new UserByIdFilterSpecification(id);

            var result = await _context.Users.WithSpecification(specification)
                .ProjectTo<BasketDTO>(_mapper.ConfigurationProvider)
                .SingleOrDefaultAsync();
            return result.Products;
        }

        public async Task AddProduct(ProductBasketRequestDTO requestDTO) {
            var specification = new UserByIdFilterSpecification(requestDTO.UserId);

            var user = await _context.Users.WithSpecification(specification)
                .SingleOrDefaultAsync();

            var product = await _context.Products.FindAsync(requestDTO.ProductId);
            user.Basket.Products.Add(product);

            _context.Users.Update(user);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteProduct(ProductBasketRequestDTO requestDTO) {
            var specification = new UserByIdFilterSpecification(requestDTO.UserId);

            var user = await _context.Users.WithSpecification(specification)
                .SingleOrDefaultAsync();

            var product = await _context.Products.FindAsync(requestDTO.ProductId);
            user.Basket.Products.Remove(product);

            _context.Users.Update(user);
            await _context.SaveChangesAsync();
        }
    }
}
