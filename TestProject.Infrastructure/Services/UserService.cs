﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestProject.Application.IServices;
using TestProject.Domain.DTOs;
using TestProject.Domain.Models;
using TestProject.Infrastructure.Repositories;

namespace TestProject.Infrastructure.Services
{
    public class UserService: IUserService {
        private readonly IMapper _mapper;
        private readonly IPasswordService _passwordService;
        private readonly BaseRepository<User> _userRepository;
        private readonly BaseRepository<Basket> _basketRepository;

        public UserService(BaseRepository<User> userRepository,
            BaseRepository<Basket> basketRepository,
            IPasswordService passwordService,
            IMapper mapper) {
            _userRepository = userRepository;
            _basketRepository = basketRepository;
            _passwordService = passwordService;
            _mapper = mapper;
        }

        public async Task<ICollection<UserDTO>> GetUsers() {
            var userDTOs = await _userRepository.GetNotDeletedDTOs<UserDTO>();
            return userDTOs;
        }

        public async Task AddUser(UserDTO userDTO) {
            var user = _mapper.Map<User>(userDTO);
            user.Password = _passwordService.HashPassword(userDTO.Password);
            user.Basket = new Basket();

            await _userRepository.AddAsync(user);
            await _userRepository.SaveChangesAsync();
        }

        public async Task DeleteUser(Guid id) {
            var user = await _userRepository.GetByIdAsync(id);
            user.DeletedDate = DateTime.Now;

            await _basketRepository.DeleteAsync(user.Basket);

            await _userRepository.UpdateAsync(user);
            await _userRepository.SaveChangesAsync();
        }
    }
}
