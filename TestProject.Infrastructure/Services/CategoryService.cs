﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestProject.Application.IServices;
using TestProject.Domain.DTOs;
using TestProject.Domain.Models;
using TestProject.Infrastructure.Repositories;

namespace TestProject.Infrastructure.Services
{
    public class CategoryService: ICategoryService {
        private readonly BaseRepository<Category> _categoryRepository;
        private readonly IMapper _mapper;

        public CategoryService(BaseRepository<Category> categoryRepository, IMapper mapper) {
            _categoryRepository = categoryRepository;
            _mapper = mapper;
        }

        public async Task<ICollection<CategoryDTO>> GetCategories() {
            var categoryDTOs = await _categoryRepository.GetNotDeletedDTOs<CategoryDTO>();
            return categoryDTOs;
        }

        public async Task AddCategory(CategoryDTO categoryDTO) {
            var category = _mapper.Map<Category>(categoryDTO);

            await _categoryRepository.AddAsync(category);
            await _categoryRepository.SaveChangesAsync();
        }

        public async Task DeleteCategory(Guid id) {
            var category = await _categoryRepository.GetByIdAsync(id);
            category.DeletedDate = DateTime.Now;

            await _categoryRepository.UpdateAsync(category);
            await _categoryRepository.SaveChangesAsync();
        }
    }
}
