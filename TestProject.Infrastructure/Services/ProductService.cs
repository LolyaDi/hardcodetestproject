﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestProject.Application.IServices;
using TestProject.Domain.DTOs;
using TestProject.Domain.Models;
using TestProject.Domain.RequestDTOs;
using TestProject.Infrastructure.Repositories;
using TestProject.Infrastructure.Specifications;

namespace TestProject.Infrastructure.Services
{
    public class ProductService: IProductService {
        private readonly BaseRepository<Product> _productRepository;
        private readonly BaseRepository<Category> _categoryRepository;
        private readonly IMapper _mapper;

        public ProductService(BaseRepository<Product> productRepository, 
            BaseRepository<Category> categoryRepository, 
            IMapper mapper) {
            _productRepository = productRepository;
            _categoryRepository = categoryRepository;
            _mapper = mapper;
        }

        public async Task<ICollection<ProductDTO>> GetProducts() {
            var productDTOs = await _productRepository.GetNotDeletedDTOs<ProductDTO>();
            return productDTOs;
        }

        public async Task<ICollection<ProductDTO>> GetFilteredProducts(FilterProductDTO filterDTO) {
            var categoryNamesFilterSpec = new ProductsByCategoryNamesFilterSpecification(filterDTO.CategoryNames);
            var parametersFilterSpec = new ProductsByParametersFilterSpecification(filterDTO.Parameters);

            var categoriesFilterResult = await _productRepository.ListAsync(categoryNamesFilterSpec);
            var products = parametersFilterSpec.Evaluate(categoriesFilterResult);

            var productDTOs = _mapper.Map<ICollection<ProductDTO>>(products);
            return productDTOs;
        }

        public async Task AddProduct(ProductDTO productDTO) {
            var product = _mapper.Map<Product>(productDTO);

            var namesFilterSpec = new CategoriesByNameFilterSpecification(productDTO.CategoryNames);
            var categories = await _categoryRepository.ListAsync(namesFilterSpec);

            product.Categories = categories;

            await _productRepository.AddAsync(product);
            await _productRepository.SaveChangesAsync();
        }

        public async Task DeleteProduct(Guid id) {
            var product = await _productRepository.GetByIdAsync(id);
            product.DeletedDate = DateTime.Now;

            await _productRepository.UpdateAsync(product);
            await _productRepository.SaveChangesAsync();
        }
    }
}
