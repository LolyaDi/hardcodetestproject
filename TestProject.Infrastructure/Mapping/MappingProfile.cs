﻿using AutoMapper;
using System.Linq;
using TestProject.Domain.DTOs;
using TestProject.Domain.Models;

namespace TestProject.Infrastructure.Mapping
{
    public class MappingProfile: Profile {
        public MappingProfile() {
            CreateMap<Category, CategoryDTO>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.ParameterNames, opt => opt.MapFrom(src => src.ParameterNames.Select(p => new ParameterNameDTO() { Id = p.Id, Name = p.Name })));
            CreateMap<CategoryDTO, Category>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.ParameterNames, opt => opt.MapFrom(src => src.ParameterNames.Select(p => new ParameterName() { Name = p.Name })))
                .ForMember(dest => dest.Id, opt => opt.Ignore());

            CreateMap<ParameterValue, ParameterDTO>()
                .ForMember(dest => dest.CategoryName, opt => opt.MapFrom(src => src.ParameterName.Category.Name))
                .ForMember(dest => dest.ParameterName, opt => opt.MapFrom(src => src.ParameterName.Name))
                .ForMember(dest => dest.ParameterNameId, opt => opt.MapFrom(src => src.ParameterNameId))
                .ForMember(dest => dest.ParameterValue, opt => opt.MapFrom(src => src.Value));
            CreateMap<ParameterDTO, ParameterValue>()
                .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.ParameterValue))
                .ForMember(dest => dest.ParameterNameId, opt => opt.MapFrom(src => src.ParameterNameId))
                .ForMember(dest => dest.ParameterName, opt => opt.Ignore());

            CreateMap<Product, ProductDTO>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.PhotoUrl, opt => opt.MapFrom(src => src.PhotoUrl))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.Price))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.CategoryNames, opt => opt.MapFrom(src => src.Categories.Select(c => c.Name)))
                .ForMember(dest => dest.Parameters, opt => opt.MapFrom(src => src.ParameterValues));
            CreateMap<ProductDTO, Product>()
                .ForMember(dest => dest.PhotoUrl, opt => opt.MapFrom(src => src.PhotoUrl))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.Price))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.ParameterValues, opt => opt.MapFrom(src => src.Parameters))
                .ForMember(dest => dest.Id, opt => opt.Ignore());

            CreateMap<User, UserDTO>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.Username))
                .ForMember(dest => dest.Password, opt => opt.MapFrom(src => src.Password));
            CreateMap<UserDTO, User>()
                .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.Username))
                .ForMember(dest => dest.Id, opt => opt.Ignore());

            CreateMap<User, BasketDTO>()
                .ForMember(dest => dest.Products, opt => opt.MapFrom(src => src.Basket.Products))
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id));
        }
    }
}
