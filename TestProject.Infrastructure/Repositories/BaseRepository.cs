﻿using Ardalis.Specification.EntityFrameworkCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestProject.Domain.DataAccess;
using TestProject.Domain.Models;
using TestProject.Infrastructure.Specifications;

namespace TestProject.Infrastructure.Repositories
{
    public class BaseRepository<T>: RepositoryBase<T> where T: Entity{
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public BaseRepository(DataContext context, IMapper mapper): base(context) {
            _context = context;
            _mapper = mapper;
        }
        
        public async Task<ICollection<TDto>> GetNotDeletedDTOs<TDto>() {
            var specification = new EntityIsBeingDeletedFilterSpecification<T>();

            var dtos = await _context.Set<T>()
                .WithSpecification(specification)
                .ProjectTo<TDto>(_mapper.ConfigurationProvider)
                .ToListAsync();
            return dtos;
        }
    }
}
