﻿using Ardalis.Specification;
using System.Collections.Generic;
using TestProject.Domain.Models;

namespace TestProject.Infrastructure.Specifications
{
    public class CategoriesByNameFilterSpecification : Specification<Category> {
        public CategoriesByNameFilterSpecification(ICollection<string> categoryNames) {
            Query.Where(c => categoryNames.Contains(c.Name));
        }
    }
}
