﻿using Ardalis.Specification;
using TestProject.Domain.Models;

namespace TestProject.Infrastructure.Specifications
{
    public class EntityIsBeingDeletedFilterSpecification<T>: Specification<T> where T:Entity {
        public EntityIsBeingDeletedFilterSpecification() {
            Query.Where(e => !e.DeletedDate.HasValue);
        }
    }
}
