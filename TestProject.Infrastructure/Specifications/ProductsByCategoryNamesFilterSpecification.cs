﻿using Ardalis.Specification;
using System.Collections.Generic;
using System.Linq;
using TestProject.Domain.Models;

namespace TestProject.Infrastructure.Specifications
{
    public class ProductsByCategoryNamesFilterSpecification: Specification<Product> {
        public ProductsByCategoryNamesFilterSpecification(ICollection<string> categoryNames) {
            Query.Where(p => !p.DeletedDate.HasValue && p.Categories.Select(c => c.Name)
                    .Any(c => categoryNames.Contains(c)));
        }
    }
}
