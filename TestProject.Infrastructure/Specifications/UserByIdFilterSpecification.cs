﻿using Ardalis.Specification;
using System;
using TestProject.Domain.Models;

namespace TestProject.Infrastructure.Specifications
{
    public class UserByIdFilterSpecification: Specification<User> {
        public UserByIdFilterSpecification(Guid id) {
            Query.Where(u => !u.DeletedDate.HasValue && u.Id == id)
                .Include(u => u.Basket).ThenInclude(b => b.Products);
        }
    }
}
