﻿using Ardalis.Specification;
using System.Collections.Generic;
using System.Linq;
using TestProject.Domain.Models;

namespace TestProject.Infrastructure.Specifications
{
    public class ProductsByParametersFilterSpecification: Specification<Product> {
        public ProductsByParametersFilterSpecification(IDictionary<string, string> parameters) {
            Query.Where(p => p.ParameterValues
                    .Any(v => parameters.Contains(new KeyValuePair<string, string>(v.ParameterNameId.ToString(), v.Value))));
        }
    }
}
