﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestProject.Domain.DTOs;

namespace TestProject.Application.IServices
{
    public interface ICategoryService {
        Task<ICollection<CategoryDTO>> GetCategories();
        Task AddCategory(CategoryDTO categoryDTO);
        Task DeleteCategory(Guid id);
    }
}
