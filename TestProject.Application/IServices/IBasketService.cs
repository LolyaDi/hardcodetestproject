﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestProject.Domain.DTOs;
using TestProject.Domain.RequestDTOs;

namespace TestProject.Application.IServices
{
    public interface IBasketService {
        Task<ICollection<BasketDTO>> GetBaskets();
        Task<ICollection<ProductDTO>> GetProducts(Guid id);
        Task AddProduct(ProductBasketRequestDTO requestDTO);
        Task DeleteProduct(ProductBasketRequestDTO requestDTO);
    }
}
