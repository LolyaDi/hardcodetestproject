﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestProject.Domain.DTOs;
using TestProject.Domain.RequestDTOs;

namespace TestProject.Application.IServices
{
    public interface IProductService {
        Task<ICollection<ProductDTO>> GetProducts();
        Task<ICollection<ProductDTO>> GetFilteredProducts(FilterProductDTO filterDTO);
        Task AddProduct(ProductDTO productDTO);
        Task DeleteProduct(Guid id);
    }
}
