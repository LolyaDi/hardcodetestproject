﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TestProject.Domain.DTOs;

namespace TestProject.Application.IServices
{
    public interface IUserService {
        Task<ICollection<UserDTO>> GetUsers();
        Task AddUser(UserDTO userDTO);
        Task DeleteUser(Guid id);
    }
}
