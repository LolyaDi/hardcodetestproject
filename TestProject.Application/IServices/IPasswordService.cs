﻿namespace TestProject.Application.IServices
{
    public interface IPasswordService {
        string HashPassword(string password);
        bool IsVerifiedPassword(string password, string passwordHash);
    }
}
